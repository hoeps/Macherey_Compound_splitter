# -*- coding: UTF-8 -*-
import Levenshtein as l
import regex as re
from operation import Operation

THRESHOLD = 0

class Rule_extractor:
    def __init__(self, rules):
        self.rules_dict = {k: v for k, v in rules.items() if v.frequency > THRESHOLD}
        self.rules = self.rules_dict.values()

    @classmethod
    def from_file(cls, filename):
        with open(filename, 'r') as file:
            rules_dict = {}
            for line in file:
                if line.rstrip():
                    line = line.rstrip().lower().split('\t')
                    compound = line[0]
                    parts = [cls.delete_second(s) for s in line[1:]]
                    rules = cls.get_rules(compound, parts)
                    if rules:
                        for rule in rules:
                            if repr(rule) not in rules_dict:
                                rule.frequency = 1
                                rules_dict[repr(rule)] = rule
                            else:
                                rules_dict[repr(rule)].frequency += 1

        return cls(rules=rules_dict)

    @staticmethod
    def group_ops(lev_ops, word1, word2):
        if not lev_ops:
            return None
        current_lev_op = lev_ops[0][0]
        current_i = lev_ops[0][1]
        current_j = lev_ops[0][2]
        block_of_lev_ops = []
        list_of_blocks = []
        lev_op = None
        for lev_op in lev_ops:
            # if (lev_op[0] == current_lev_op) and ((abs(lev_op[1]-current_i)<=1) or (abs(lev_op[2]-current_j)<=1)):
            if ((lev_op[0] == current_lev_op) and (abs(lev_op[1] - current_i) <= 1)):
                morpho_rule = Operation.from_lev_op(lev_op, word1, word2)
                block_of_lev_ops.append(morpho_rule)
            else:
                list_of_blocks.append(block_of_lev_ops)
                morpho_rule = Operation.from_lev_op(lev_op, word1, word2)
                block_of_lev_ops = [morpho_rule]
        if lev_op == lev_ops[-1]:
            list_of_blocks.append(block_of_lev_ops)

        rules = []
        for block in list_of_blocks:
            rule = block[0]
            if len(block) > 1:
                for r in block[1:]:
                    rule = rule.chain_with_rule(r)
            rules.append(rule.inverse())

        if not rules:
            rules = [Operation(input='', output='', position='m')]
        return rules

    @staticmethod
    def get_rules(compound, parts):
        if not compound or not parts:
            return None
        concatted_lemmata = u''.join(parts)
        ops = l.editops(concatted_lemmata, compound)
        if (len(ops)>5):
            return None
        if ops:
            rules = Rule_extractor.group_ops(ops, concatted_lemmata, compound)
        else:
            rules = [Operation(input='', output='', position='m', proof=compound)]
        for rule in rules:
            rule.proof = compound + " | " + str(parts)
        return rules

    @staticmethod
    def delete_second(string):
        regex = re.compile(r'^(.*)\|(.*)$')
        m = regex.match(string)
        if m:
            return m[1]
        else:
            return string

    def print_rules(self, print_proof=True):
        to_print = []
        for key, value in self.rules_dict.items():
            to_print.append((key, value.proof, value.frequency))
        to_print = sorted(to_print, key=lambda x: x[2])
        for e in to_print:
            print (e)
