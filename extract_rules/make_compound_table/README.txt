==From phrase-table to composita===
python3 extract_composita.py phrase-table 2000000 > composita_untranslated
python3 translate.py lex.e2f composita_untranslated > composita

==From phrase-table to non-composita===
python3 extract_non_composita.py phrase-table > non_composita
