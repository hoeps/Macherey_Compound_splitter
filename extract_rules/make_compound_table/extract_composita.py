import sys
import regex as re

# USAGE:
# python3 extract_composita.py phrase-table 2000000 > composita_untranslated

re_num = re.compile(r'(.*)\d(.*)')
re_punc = re.compile(r'(.*)\p{p}(.*)')


def check_de(inputString):
    if re_num.match(inputString): return False
    if re_punc.match(inputString): return False
    if len(inputString) < 2: return False
    return True


blacklist_words = ['the', 'in', 'a', 'an', 'to', 'or', 'of']


def check_en(inputString):
    if re_num.match(inputString): return False
    if re_punc.match(inputString): return False
    parts_en = inputString.rstrip().lstrip().split(' ')
    if len(parts_en) == 1: return False
    for p in parts_en:
        if len(p) < 3: return False
        if p in blacklist_words: return False
    return True


if len(sys.argv) >= 3:
    limit = int(sys.argv[2])
else:
    limit = float('Inf')

with open(sys.argv[1], 'r') as phrase_file:
    ctr = 0
    for line in phrase_file:
        line = line.rstrip()
        if ctr >= limit: break
        parts_line = line.split('|||')
        parts_de = parts_line[0].rstrip().lstrip().split(' ')
        if len(parts_de) == 1 and check_de(parts_de[0]):
            if check_en(parts_line[1]):
                print(parts_de[0] + "\t" + parts_line[1].lstrip().rstrip())
        ctr += 1
