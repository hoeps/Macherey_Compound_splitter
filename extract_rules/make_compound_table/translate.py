import sys
import regex as re
import Levenshtein as l
from difflib import SequenceMatcher
import itertools
import editdistance as e

if len(sys.argv) < 3:
    print("USAGE: python3 translate.py lex.e2f composita_untranslated > composita")

MIN_PROB = 0.1


def check_distance(compositum, words_de):
    lowest_distance = float('Inf')
    best_words = None
    for words in list(itertools.product(*words_de)):
        joined_words = ''.join(words)
        if abs(len(compositum) - len(joined_words)) <= 5:
            d = e.eval(''.join(words), compositum)
            # print(words, d, sep=" ")
            if d < lowest_distance:
                lowest_distance = d
                best_words = words
    if lowest_distance < len(compositum) / float(2):
        return best_words, lowest_distance
    else:
        return False, False


re_num = re.compile(r'(.*)\d(.*)')
re_punc = re.compile(r'(.*)\p{p}(.*)')


def check_de(inputString):
    if inputString == 'null': return False
    if re_num.match(inputString): return False
    if re_punc.match(inputString): return False
    if len(inputString) < 2: return False
    return True


def print_split_dict(d):
    for k, v in d.items():
        print(k + "\t" + " ".join(v[0]))


translation_dict = {}

with open(sys.argv[1], 'r') as lex_file:
    for line in lex_file:
        line = line.rstrip().lower()
        parts = line.split(' ')
        german = parts[0]
        english = parts[1]
        prob = float(parts[2])
        if english in translation_dict:
            if prob > MIN_PROB and check_de(german):
                translation_dict[english].append((german, prob))
        else:
            translation_dict[english] = [(german, prob)]

comp_bestsplit_cost = {}
with open(sys.argv[2], 'r') as file:
    for line in file:
        parts = line.rstrip().lower().split('\t')
        compositum = parts[0]

        if len(compositum) > 6 and comp_bestsplit_cost.get(compositum, [None, float('Inf')])[1] != 0:
            words_en = parts[1].split(' ')
            words_de = [[] for i in range(len(words_en))]
            for i, w in enumerate(words_en):
                if w in translation_dict:
                    for translation in translation_dict[w]:
                        words_de[i].append(translation[0])
            best_words, distance = check_distance(compositum, words_de)
            if best_words:
                if distance < (comp_bestsplit_cost.get(compositum, [None, float('Inf')])[1]):
                    comp_bestsplit_cost[compositum] = [best_words, distance]

print_split_dict(comp_bestsplit_cost)
