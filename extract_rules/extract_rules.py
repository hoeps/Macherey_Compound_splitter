# -*- coding: UTF-8 -*-
from get_rules import Rule_extractor
import sys

if len(sys.argv) != 2:
    print ("Usage: python3 main.py <composita_candidates>")


else:
    rule_extractor = Rule_extractor.from_file(sys.argv[1])
    rule_extractor.print_rules(print_proof=True)

