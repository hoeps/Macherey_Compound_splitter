# -*- coding: UTF-8 -*-
import regex as re
import math
import itertools
import sys

INF = float("inf")
SPLIT_PENALTY = 1


class Operation:
    def __init__(self, input, output, position, proof, frequency=0):
        self.input = input
        self.output = output
        self.position = position
        self.proof = proof
        self.frequency = frequency
        if self.position == 's':
            self.regex = re.compile(r'^(' + re.escape(self.input) + r')(.*)$')
        if self.position == 'm':
            self.regex = re.compile(r'^(.*)(' + re.escape(self.input) + r')(.*)$')
        if self.position == 'e':
            self.regex = re.compile(r'^(.*)(' + re.escape(self.input) + r')$')

    @classmethod
    def from_lev_op(cls, lev_op, word1, word2):
        if lev_op[1] == 0 or lev_op[2] == 0:
            position = 's'
        elif lev_op[1] == len(word1) - 1 or lev_op[2] == len(word2) - 1:
            position = 'e'
        else:
            position = 'm'
        if lev_op[0] == 'insert':
            letter = word2[lev_op[2]]
            return cls(input='', output=letter, position=position, proof=word1)
        elif lev_op[0] == 'delete':
            letter = word1[lev_op[1]]
            return cls(input=letter, output='', position=position, proof=word1)
        else:
            letter_in = word1[lev_op[1]]
            letter_out = word2[lev_op[2]]
            return cls(input=letter_in, output=letter_out, position=position, proof=word1)

    def inverse(self):
        return Operation(input=self.output, output=self.input, position=self.position, proof=self.proof,
                         frequency=self.frequency)

    def apply(self, string, pos1, pos2):
        match = self.regex.match(string)
        if match:
            if (self.position == 's' and pos1 == 's'):
                return self.regex.sub(r're.escape(self.output)' + r'\2', string)
            elif (self.position == 'e' and pos2 == 'e'):
                return self.regex.sub(r'\1' + re.escape(self.output), string)
            elif (self.position == 'm'):
                return self.regex.sub(r'\1' + re.escape(self.output) + r'\3', string)
            else:
                return string
        return string

    def chain_with_rule(self, morpho_rule):
        new_in = self.input + morpho_rule.input
        new_out = self.output + morpho_rule.output
        if (self.position == 's' or morpho_rule.position == 's'):
            position = 's'
        elif (self.position == 'e' or morpho_rule.position == 'e'):
            position = 'e'
        else:
            position = 'm'
        return Operation(input=new_in, output=new_out, position=position, proof=self.proof, frequency=1)

    def __repr__(self):
        return u'{}/{} --{}'.format(self.output, self.input, self.position)

