import gensim
import glob
import sys
import nltk
import logging

class CorpusReader:
    def __init__(self, filename):
        self.filename = filename
        self.tokenizer = nltk.word_tokenize

    def __iter__(self):
        for line in open(self.filename, "r"):
            #yield self.tokenizer(line)
            yield line.split(" ")

if len(sys.argv) == 3:
    corpus_file = sys.argv[1]
    outfile = sys.argv[2]
else:
    corpus_file = "/home/rh/Studium/Masterarbeit/Data/Cap/europarl-v3.de.tok.true"
    outfile = "./embedding/embeddings_europarl"

# model = gensim.models.KeyedVectors.load_word2vec_format('./GoogleNews-vectors-negative300.bin.gz', binary=True)
# model = gensim.models.Word2Vec.load('./embeddings')

dataset = CorpusReader(corpus_file)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
model = gensim.models.Word2Vec(dataset, size=500, window=5, min_count=3, negative=5, workers=15, sg=1)
model.save(outfile)
