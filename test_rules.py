import os
import unittest

from rules import RulesCompiler, MorphoRule
from splitter import Splitter
from functions import get_unigrams
 
class TestSplitter(unittest.TestCase):
 
    def setUp(self):
        self.rc = RulesCompiler.from_ops()
        self.unigram_counts = get_unigrams(os.environ['HIML']+"unigrams/unigrams_true.txt")
        self.splitter = Splitter(self.rc.rules, self.unigram_counts)

    def testRC(self):
        self.assertEqual((len(self.rc.rules)), 24)
    



class TestRules(unittest.TestCase):
    def setUp(self):
        self.morphoRule1 = MorphoRule(input="er", output="", position='m',
            umlautierung_allowed=True, umlautierung_char='ä')
        self.morphoRule2 = MorphoRule(input="s", output="", position='m',
            umlautierung_allowed=False)
        self.morphoRule3 = MorphoRule(input="en", output="on", position="m",
            umlautierung_allowed=False)

    def test_apply(self):        
        result1 = self.morphoRule1.apply("Häuser", 's', 'm')
        self.assertEqual(result1, "Häus")
        result2 = self.morphoRule2.apply("Staats", 's', 'm')
        self.assertEqual(result2, "Staat")
        result21 = self.morphoRule2.apply("sfeind", 'm', 'e')
        self.assertEqual(result21, "sfeind")
        result3 = self.morphoRule3.apply("stadien", 'm', 'm')
        self.assertEqual(result3, "stadion")
        result4 = self.morphoRule2.apply("staats", 'm', 'e')
        self.assertEqual(result4, "staats")

    def test_apply_with_umlautierung(self): 
        result1 = self.morphoRule1.apply_with_umlautierung("Häuser", 's', 'm')
        self.assertEqual(result1, "Haus")
        result2 = self.morphoRule2.apply_with_umlautierung("Staats", 's', 'm')
        self.assertEqual(result2, "Staat")




if __name__ == '__main__':
    unittest.main()