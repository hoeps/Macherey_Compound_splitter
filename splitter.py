# -*- coding: UTF-8 -*-
import regex as re
import math
import itertools
import sys
from rules import *
import nltk
from itertools import combinations
import gensim
from textblob_de.lemmatizers import PatternParserLemmatizer

INF = float("inf")

COST_UNSEEN = INF
COST_RULE_APPLICATION = 0.01

SPLIT_PENALTY = -8
EMBED_WEIGHT = 0.00015

MIN_LENGTH = 3

POST_LEMMATIZE = 0
DEBUG = 0

class Splitter:
    # We want the rules for decompounding
    def __init__(self, decompound_rules, unigram_counts, mode, embed_file=False):

        self.decompound_rules = decompound_rules
        # elements of unigram_counts look like this: lowercased_word:[count, truecased_word]
        self.total_count = self.get_total_count(unigram_counts)
        self.unigram_counts = unigram_counts
        self.vocab_size = len(unigram_counts.items())
        self.normalize_counts()
        self.empty_op = MorphoRule(input="", output="")
        self.mode = mode
        self.word2vec_model = None
        if embed_file:
            sys.stderr.write("Loading word embeddings from file: "+embed_file+" ...\n")
            self.word2vec_model = gensim.models.Word2Vec.load(embed_file)
            self.embed_weight = EMBED_WEIGHT
        self.current_word = ""


    def get_total_count(self, unigram_counts):
        count = 0
        for lemma, (freq, true) in unigram_counts.items():
            count += freq
        return count

    def normalize_counts(self):
        for lemma, (freq, true) in self.unigram_counts.items():
            self.unigram_counts[lemma] = (freq/self.total_count, true)
        

    def truecase(self, w):
        w = w.lower()
        w_truecased = self.unigram_counts.get(w, [None, w])[1]
        return w_truecased

    def umlautierung(self, w):
        more_gs=set()
        if re.match(r'(.*)ö(.*)', w):
            more_gs.add(re.sub('ö', 'o', w, 1))
        if re.match(r'(.*)ä(.*)', w):
            more_gs.add(re.sub('ä', 'a', w, 1))
        if re.match(r'(.*)ü(.*)', w):
            more_gs.add(re.sub('ü', 'u', w, 1))
        return more_gs

    def get_possible_gs(self, v, pos1, pos2):
        gs = set()
        gs.add((v, 0, self.empty_op))
        if len(v) < MIN_LENGTH:
            return list(gs)
        for rule in self.decompound_rules:
            result = rule.apply(v, pos1, pos2)
            if result:
                gs.add((result, COST_RULE_APPLICATION, (rule)))     
        return list(gs)

    def cosine_to_prob(self, cosine_sim):
        #a = 3.86211609386
        #b = 1.83439776421e-14
        #pw = 20

        a = 0.66
        b = 0.07
        pw = 5

        return ((cosine_sim+a)**pw)*b

    def get_best_g_cost(self, v, pos1, pos2):
        lowest_cost = INF
        best_g = 'xxxx'
        used_rules = 'xxxx'
        embed_prob = '-'
        count_prob = '-'
        #if v == "zugeschnittene":
            #debug1 = self.truecase(self.current_word)
            #debug2 = self.truecase(self.current_word) not in self.word2vec_model.wv.vocab
        for (g, rule_cost, rules) in self.get_possible_gs(v, pos1, pos2):
            if g.lower() in self.unigram_counts:
                count_prob = self.unigram_counts[g.lower()][0]
                if (not self.word2vec_model
                        or self.truecase(v) not in self.word2vec_model.wv.vocab
                        or self.truecase(self.current_word) not in self.word2vec_model.wv.vocab
                        or self.truecase(v) == self.truecase(self.current_word)
                        or (pos1 == 'm' and pos2 == 'm')):
                    cost = -math.log(count_prob)

                else:
                    try:
                        cosine_sim = self.word2vec_model.similarity(self.truecase(v), self.truecase(self.current_word))
                        embed_prob = self.cosine_to_prob(cosine_sim)
                        cost = -math.log(self.embed_weight*embed_prob+(1-self.embed_weight)*(count_prob))
                        #cost = -math.log(self.embed_weight * embed_prob) - math.log((1 - self.embed_weight) * (count_prob))
                    except ValueError:
                        sys.stderr.write("current_word: "+str(self.current_word)+"\n")
                        sys.stderr.write("v: " + str(v) + "\n")
                        sys.stderr.write("embed_prob: " + str(embed_prob) + "\n")
                        sys.stderr.write("count_prob: " + str(count_prob) + "\n")
            else:
                cost = COST_UNSEEN
            cost += rule_cost

            if DEBUG:
                print(g.lower(), "costs", cost, "embed_prob:", embed_prob, "count_prob:", count_prob, sep=" ")

            if (lowest_cost is None) or (cost < lowest_cost):
                lowest_cost = cost
                best_g = g
                used_rules = rules
            if DEBUG:
                print("lowest_cost =", lowest_cost, sep="")
        if DEBUG:
            print("best_g =", best_g, sep=" ")
        return (best_g, lowest_cost, used_rules)

    def split_algorithm(self, w):
        self.current_word = w
        N = len(w)
        Q = [INF] * (N + 1)
        Q[0] = 0
        B = [None] * (N + 1)
        Q[0] = 0
        for i in range(N):
            pos1 = 's' if i == 0 else 'm'
            for j in range(i + 1, N + 1):
                if DEBUG>0: print ("inspecting", w[i:j])
                pos2 = 'e' if j == N else 'm'
                (g, cost, used_rules) = self.get_best_g_cost(w[i:j], pos1, pos2)
                #split_costs = Q[i] + cost + SPLIT_PENALTY
                split_costs = Q[i] + cost + SPLIT_PENALTY
                if DEBUG > 1: print (split_costs)
                if split_costs < Q[j]:
                    if DEBUG > 0: print ("=======splitting here costs: "+str(split_costs)+"=======")
                    Q[j] = split_costs
                    B[j] = (i, g, used_rules, split_costs)
        return B

    def split_to_lemmata(self, w):
        w=w.rstrip()
        if len(w)<=4:
            return ([w], [self.empty_op], "-")
        #w = w.lower()
        B = self.split_algorithm(w)
        if B and B[-1] is not None:
            splitpoints = []
            lemmata = []
            all_used_rules = []
            all_costs = []
            (s, l, used_rules, cost) = B[-1]
            splitpoints.append(s)
            lemmata.append(l)
            all_used_rules.append(used_rules)
            all_costs.append(cost)
            while (s != 0):
                (s, l, used_rules, cost) = B[s]
                splitpoints.append(s)
                lemmata.append(l)
                all_used_rules.append(used_rules)
                all_costs.append(cost)
            lemmata = lemmata[::-1]
            all_used_rules = all_used_rules[::-1]
            all_costs = ['%.2f' % elem for elem in all_costs[::-1]]
            splitpoints.sort()
            offset = 0
            w_split = w
            for s in splitpoints:
                if s != 0:
                    w_split = w_split[:s + offset] + '|' + w_split[s + offset:]
                    offset += 1
            #return (w_split, lemmata, all_used_rules, all_costs)

            if len(splitpoints) == 1:
                return ([w], all_used_rules, all_costs)
            lemmata_truecased = [self.truecase(lem) for lem in lemmata]
            if POST_LEMMATIZE:
                lemmatizer = PatternParserLemmatizer()
                actual_lemmata = [lemmatizer.lemmatize(lem)[0][0] for lem in lemmata_truecased]
                ret_tokens = actual_lemmata[0:-1]
                ret_tokens.append(lemmata_truecased[-1])
                return (ret_tokens, all_used_rules, all_costs)
            else:
                return (lemmata_truecased, all_used_rules, all_costs)
        else:
            return ([w], [self.empty_op], "-")

    def split_to_lemmata_string(self,w):
        (lemmata, _, _) = self.split_to_lemmata(w)
        return "\t".join(lemmata)

    def split_to_lemmata_string_with_rules(self,w):
        (lemmata, all_used_rules, _) = self.split_to_lemmata(w)
        retstring = ""
        for lemma, rule in zip(lemmata, all_used_rules):
            retstring+=lemma+" "+rule.get_string_rep_inverse()+" "
        return re.sub(" / $", "", retstring)

    def segment(self, w):
        (lemmata, all_used_rules, _) = self.split_to_lemmata(w)
        retstring = ""
        for lemma, rule in zip(lemmata, all_used_rules):
            result = rule.apply_inverse(lemma)
            if result:
                retstring += (result+" ")
            '''
            if rule.output=="":
                #if not rule.input=="":
                    #retstring += re.sub("--.", "", str(rule.inverse()))
                retstring += lemma+" "
            else:
                retstring += lemma+" "
                #retstring += re.sub("--.", "", str(rule.inverse()))
            '''
        return retstring.rstrip()

    def main_split(self, w):
        if self.mode == "lemmata_rules":
            return self.split_to_lemmata_string_with_rules(w)
        if self.mode == "lemmata":
            return self.split_to_lemmata_string(w)
        if self.mode == "segment":
            return self.segment(w)
        if self.mode == "lemmata_rules_cost":
            return self.split_to_lemmata(w)

    def sentence_split(self, sent):
        retwords = []
        for w in nltk.word_tokenize(sent):
            (lemmata, all_used_rules, _) = self.split_to_lemmata(w)
            if len(lemmata) == 1:
                retwords.append(lemmata[0])
            else:
                retwords.append("<cmp>")
                retwords.append(self.split_to_lemmata_string_with_rules(w))
                retwords.append("</cmp>")
        return " ".join(retwords)

    def find_possible_segmentations(self, word):
        cuts = []
        segmentations = []
        for i in range(0,len(word)):
            cuts.extend(combinations(range(1, len(word)), i))
        for i in cuts:
            last = 0
            seg = []
            for j in i:
                seg.append(word[last:j])
                last = j
            seg.append(word[last:])
            segmentations.append(seg)
        for seg in segmentations:
            proof = self.check_seg(seg)
            if proof:
                print(seg)
                print(proof)
                print("---")

    def check_seg(self, segmentation):
        #print("checking ", segmentation, sep=" ")
        proof = []
        for token in segmentation:                
            lemmata=self.get_possible_gs(token,"m", "m")
            if lemmata:
                l_found = False
                for l in lemmata:
                    if not l_found:
                        if l[0] in self.unigram_counts:
                            proof.append(l[0])
                            l_found = True

                if not l_found: return False
            else:
                return False
        return proof