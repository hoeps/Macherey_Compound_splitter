# -*- coding: UTF-8 -*-
from splitter import Splitter
from rules import RulesCompiler
from functions import *
import sys
import readline
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("--interactive_europarl", help="interactive mode trained on europarl data", default=False, action='store_true')
parser.add_argument("--interactive_medical", help="interactive mode trained on europarl data", default=False, action='store_true')
parser.add_argument("--interactive", help='interactive mode. unigrams and embeddings must be specified', default=False, action='store_true')
parser.add_argument("--counts", help="Path to the unigram count file", type=str,
                    default="data/unigrams/unigrams_himl_medical.txt")

parser.add_argument("--interactive_sentence", help='interactive sentence input', default=False, action='store_true')
parser.add_argument("--on_file", help='path to compound file', type=str, default="data/gold_standard/gold_medical.txt")
parser.add_argument("--split_mode", help='The format of the output', type=str, default="lemmata_rules",
                    choices=["segment", "lemmata", "lemmata_rules", "lemmata_rules_cost"])
parser.add_argument("--find_all_splits", help='experimentally find all possible splits. Note, that the actual splitter does NOT do that.', default=False, action='store_true')
parser.add_argument("--embed_file", help='path to embedding_file', default=False)
parser.add_argument("--rules", help='the set of rules that is used', type=str, default="langer",
                    choices=["langer", "langer_all", "langer_most_frequent", "learnt_medical", "learnt_europarl"] )


parser.add_argument("--on_entailment_file", default=False)
args = parser.parse_args()

if args.interactive_europarl:
    args.counts = "data/unigrams/europarl_unigrams.txt"
    args.embed_file = "./embedding/embeddings_europarl"
    args.interactive = True

if args.interactive_medical:
    args.counts = "data/unigrams/unigrams_himl_medical.txt"
    args.embed_file = "./embedding/embeddings_medical"
    args.interactive = True

if args.on_entailment_file:
    args.counts = "data/unigrams/europarl_unigrams.txt"
    args.embed_file = "./embedding/embeddings_europarl"
    args.split_mode = "lemmata"

r = RulesCompiler.from_ops(args.rules)
unigram_counts = get_unigrams(args.counts)


s = Splitter(r.rules, unigram_counts, args.split_mode, args.embed_file)

if args.find_all_splits:
    find_possible_splits(s)

elif args.interactive:
    decompound_interactive(s)

elif args.interactive_sentence:
    decompound_interactive_sentence(s)

elif args.on_entailment_file:
    decompound_entailment_file(args.on_entailment_file, s)

elif not args.interactive:
    decompound_file(args.on_file, s)

