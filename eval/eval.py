import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("output_file", help="The output_file of a splitter to be evaluated", type=str)
parser.add_argument("--gold_standard", help='path to gold standard file', type=str,
                    default=os.environ['HIML'] + "gold_compounds/gold_from_tail.txt")
parser.add_argument("--print_wrong", action='store_true', default=False)
args = parser.parse_args()


def matching(parts, parts_gold):
    for w, w_gold in zip(parts, parts_gold):
        w_gold_options = w_gold.split("|")
        match = False
        for option in w_gold_options:
            if w == option:
                match = True
        if not match:
            return False
    return True


if args.output_file:
    correct_splits = 0
    correct_non_splits = 0
    wrong_non_splits = 0
    wrong_faulty_splits = 0
    wrong_splits = 0
    total_samples = 0.0
    compounds = 0.0
    non_compounds = 0.0

    for line, line_gold in zip(open(args.output_file), open(args.gold_standard)):
        components = line.split("\t")
        word = components[0]
        parts = components[1:]
        parts = [p.strip() for p in parts]

        components_gold = line_gold.split("\t")
        word_gold = components_gold[0]
        parts_gold = components_gold[1:]
        parts_gold = [p.rstrip() for p in parts_gold]

        if word and word_gold:
            total_samples += 1
            if word != word_gold:
                print("Error! Not the same words in files. Found", "\'" + str(word) + "\'", "and",
                      "\'" + str(word_gold) + "\'", sep=" ")
            if matching(parts, parts_gold) and len(parts) > 1:
                # no. of compounds that are split correctly.
                correct_splits += 1
            elif matching(parts, parts_gold) and len(parts) == 1:
                # no. of non-compounds that are not split.
                correct_non_splits += 1
            elif len(parts_gold) > 1 and len(parts) == 1:
                # no. of compounds that are not split.
                wrong_non_splits += 1
                if args.print_wrong:
                    print("wrong_non_split. Gold: ", word, parts_gold, sep=" ")
                    print("we have:", parts, sep=" ")
                    print("-----------------------")
            elif len(parts_gold) > 1 and len(parts) > 1 and not matching(parts, parts_gold):
                # no. of compounds that are split, but incorrectly.
                wrong_faulty_splits += 1
                if args.print_wrong:
                    print("wrong_faulty_split. Gold: ", word, parts_gold, sep=" ")
                    print("we have:", parts, sep=" ")
                    print("-----------------------")
            elif len(parts_gold) == 1 and len(parts) > 1:
                # no. of non-compounds that are split.
                wrong_splits += 1
                if args.print_wrong:
                    print("wrong_split. Gold: ", word, parts_gold, sep=" ")
                    print("we have:", parts, sep=" ")
                    print("-----------------------")
            else:
                wrong_splits += 1
                print("a strange wrong split found:", str(parts_gold), str(parts), sep=" ")

    precision = correct_splits / (correct_splits + wrong_faulty_splits + wrong_splits)
    recall = correct_splits / (correct_splits + wrong_faulty_splits + wrong_non_splits)

    correct = correct_splits + correct_non_splits
    wrong = wrong_faulty_splits + wrong_splits + wrong_non_splits
    accuracy = correct / (correct + wrong)
    print("total_samples:", total_samples)
    print("correct_splits:", correct_splits,"|", correct_splits/total_samples,"%", sep=" ")
    print("correct_non_splits:", correct_non_splits,"|", correct_non_splits/total_samples,"%", sep=" ")
    print("wrong_non_splits:", wrong_non_splits,"|", wrong_non_splits/total_samples,"%", sep=" ")
    print("wrong_faulty_splits:", wrong_faulty_splits,"|", wrong_faulty_splits/total_samples,"%", sep=" ")
    print("wrong_splits", wrong_splits,"|", wrong_splits/total_samples,"%", sep=" ")
    print("--------------")

    print("Precision:", precision, sep=" ")
    print("Recall:", recall, sep=" ")
    print("F1-score:", 2 * (precision * recall) / (precision + recall), sep=" ")
    print("Accuracy:", accuracy, sep=" ")
