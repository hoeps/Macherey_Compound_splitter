import regex as re


LANGER_ALL = [# Fugenelemente
              ("s", "", "m", 0), # Staatsfeind
              ("n", "", "m", 0), # Soziologenkongress
              ("en", "", "m", 0), # Straußenei
              ("nen", "", "m", 0), # Wöchnerinnenheim
              ("e", "", "m", 0), # Hundehalter
              ("e", "", "m", 1), # Gänseklein
              ("es", "", "m", 0), # Geisteshaltung
              ("-", "", "m", 0), # Französisch-Deutsch
              ("er", "", "m", 0), # Geisterstunde
              ("er", "", "m", 1), # Häuserreihe
              ("ien", "", "m", 0), # Prinzipienreiter
              # Negative Fugenelemente              
              ("", "e", "m", 0), # Kirchhof
              ("", "en", "m", 0), # Südwind
              # Tail Substitution
              ("en", "us", "m", 0), # Aphorismenschatz
              ("en", "um", "m", 0), # Museenverwaltung
              ("a", "um", "m", 0), # Aphrodisiakaverkäufer              
              ("en", "a", "m", 0), # Madonnenkult   
              ("en", "on", "m", 0), # Stadienverbot
              ("a", "on", "m", 0), # Pharmakaanalyse
              ("e", "en", "m", 0)] # Schweigeminute

LEARNT_RULES_MEDICAL = [("s", "", "m", 0),
                ("", "e", "m",0),
                ("n", "", "m",0),
                ("", "i", "m",0),]

LEARNT_RULES_EUROPARL = [("s", "", "m", 0),
                ("", "e", "m",0),
                ("", "i", "m",0),
                ("", "t", "m",0)]

MOST_FREQUENT_LANGER_OPS = [("s", "", "m", 0),
                            ("es", "", "m", 0),
                            ("n", "", "m", 0),
                            ("e", "", "m", 0),
                            ("en", "", "m", 0),
                            #("", "en", "m", 0),
                            ("-", "", "m", 0)]

LANGER = [# Fugenelemente
              ("s", "", "m", 0), # Staatsfeind
              ("n", "", "m", 0), # Soziologenkongress
              ("en", "", "m", 0), # Straußenei
              ("nen", "", "m", 0), # Wöchnerinnenheim
              ("e", "", "m", 0), # Hundehalter
              ("e", "", "m", 1), # Gänseklein
              ("es", "", "m", 0), # Geisteshaltung
              ("-", "", "m", 0), # Französisch-Deutsch
              ("er", "", "m", 0), # Geisterstunde  | PERFORMING BAD WITH CAP
              ("er", "", "m", 1), # Häuserreihe | PERFORMING BAD WITH CAP
              ("ien", "", "m", 0), # Prinzipienreiter
              # Negative Fugenelemente
              #("", "e", "m", 0), # Kirchhof | PERFORMING BAD WITH CAP
              #("", "en", "m", 0), # Südwind | PERFORMING BAD WITH CAP
              # Tail Substitution
              ("en", "us", "m", 0), # Aphorismenschatz
              ("en", "um", "m", 0), # Museenverwaltung
              ("a", "um", "m", 0), # Aphrodisiakaverkäufer
              ("en", "a", "m", 0), # Madonnenkult
              ("en", "on", "m", 0), # Stadienverbot
              ("a", "on", "m", 0), # Pharmakaanalyse
              ("e", "en", "m", 0) # Schweigeminute
            ]

class MorphoRule:
    def __init__(self, input, output, example=None, frequency=0, umlautierung=False, umlautierung_char=None):
        self.input = input
        self.output = output
        self.example = example
        self.frequency = frequency
        self.umlautierung = umlautierung
        self.umlautierung_char = umlautierung_char

        #if self.position == 's':
            #self.regex = re.compile(r'^(' + re.escape(self.input) + r')(.*)$')

        self.regex = re.compile(r'^(.*)(' + re.escape(self.input) + r')$')
        #if self.position == 'e':
            #self.regex = re.compile(r'^(.*)(' + re.escape(self.input) + r')$')
        self.inverse_regex = None

    @classmethod
    def from_string(cls, rule_string):
        regex_uml = re.compile(r'(.)/(.)+(.*)/(.*)')
        regex_no_uml = re.compile(r'(.*)/(.*)')
        match = regex_uml.match(rule_string)
        if match:
            return cls(input=match.group(3), output=match.group(4), umlautierung=True, umlautierung_char=match.group(2))
        else:
            match = regex_no_uml.match(rule_string)
            if match:
                return cls(input=match.group(1), output=match.group(2))
        return None



    def apply_no_uml(self, string, pos1, pos2):
        # Application without Umlautierung
        match = self.regex.match(string)
        if match and pos2 != 'e':
            #return self.regex.sub(r'\1' + re.escape(self.output), string)
            return self.regex.sub(r'\1' + self.output, string)
        return None


    def apply(self, string, pos1='m', pos2='m'):
        first_apply = self.apply_no_uml(string, pos1, pos2)
        if not self.umlautierung: 
            return first_apply
        else:
            if first_apply:        
                if self.umlautierung_char == 'ö' and re.match(r'(.*)ö(.*)', first_apply):
                    return re.sub('ö', 'o', first_apply, 1)
                if self.umlautierung_char == 'ä' and re.match(r'(.*)ä(.*)', first_apply):
                    return re.sub('ä', 'a', first_apply, 1)
                if self.umlautierung_char == 'ü' and re.match(r'(.*)ü(.*)', first_apply):
                    return re.sub('ü', 'u', first_apply, 1)
        return None
       
  
    def __repr__(self):
        if self.umlautierung:
            if self.umlautierung_char=='ä':
                return u'ä/a+{}/{}'.format(self.input, self.output)
            if self.umlautierung_char=='ö':
                return u'ö/o+{}/{}'.format(self.input, self.output)
            if self.umlautierung_char=='ü':
                return u'ü/u+{}/{}'.format(self.input, self.output)
        else:    
            return u'{}/{}'.format(self.input, self.output)

    def get_string_rep_inverse(self):
        if self.umlautierung:
            if self.umlautierung_char=='ä':
                return u'a/ä+{}/{}'.format(self.output, self.input)
            if self.umlautierung_char=='ö':
                return u'o/ö+{}/{}'.format(self.output, self.input)
            if self.umlautierung_char=='ü':
                return u'o/ü+{}/{}'.format(self.output, self.input)  
            return "<strange-rule>"          
        else:    
            return u'{}/{}'.format(self.output, self.input)

    def apply_inverse_no_uml(self, string):
        # Application without Umlautierung
        if self.inverse_regex is None:
            self.inverse_regex = re.compile(r'^(.*)(' + re.escape(self.output) + r')$')
        match = self.inverse_regex.match(string)
        if match:
            return self.inverse_regex.sub(r'\1' + re.escape(self.input), string)
        return None

    def apply_inverse(self, string):
        first_apply = self.apply_inverse_no_uml(string)
        if not self.umlautierung: 
            return first_apply
        else:            
            if self.umlautierung_char == 'ö' and re.match(r'(.*)o(.*)', first_apply):
                return re.sub('o', 'ö', first_apply, 1)
            if self.umlautierung_char == 'ä' and re.match(r'(.*)a(.*)', first_apply):
                return re.sub('a', 'ä', first_apply, 1)
            if self.umlautierung_char == 'ü' and re.match(r'(.*)u(.*)', first_apply):
                return re.sub('u', 'ü', first_apply, 1)
            return None


class RulesCompiler():
    def __init__(self, rules):
        self.rules = rules

    @classmethod
    def from_rules_string_list(cls, rules_string_list):
        rules=[]
        for (i, o, p, uml) in rules_string_list:
            if uml:
                for c in ['ö', 'ä', 'ü']:
                    rules.append(MorphoRule(input=i, output=o, umlautierung=True, umlautierung_char=c))
                rules.append(MorphoRule(input=i, output=o, umlautierung=False))
            else:
                rules.append(MorphoRule(input=i, output=o))
        return cls(rules=rules)

    @classmethod
    def from_ops(cls, mode):
        if mode=="langer_all":
            return cls.from_rules_string_list(LANGER_ALL)
        elif mode=="langer_most_frequent":
            return cls.from_rules_string_list(MOST_FREQUENT_LANGER_OPS)
        elif mode=="learnt_medical":
            return cls.from_rules_string_list(LEARNT_RULES_MEDICAL)
        elif mode=="learnt_europarl":
            return cls.from_rules_string_list(LEARNT_RULES_EUROPARL)
        elif mode=="langer":
            return cls.from_rules_string_list(LANGER)