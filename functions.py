# -*- coding: UTF-8 -*-
import regex as re
import sys
import nltk

MIN_LEMMA_LENGTH = 3
MIN_FREQ = 10


def get_unigrams(filename):
    unigrams = {}
    with open(filename, 'r') as file:
        for line in file:
            line = line.rstrip()
            parts = line.split('\t')
            lemma = parts[0]
            frequency = float(parts[1])
            truecased = parts[2]
            if len(lemma) >= MIN_LEMMA_LENGTH:
                if lemma in unigrams:
                    if unigrams[lemma] < frequency:
                        unigrams[lemma] = frequency
                else:
                    unigrams[lemma] = [float(frequency), truecased]

    for lemma in list(unigrams.keys()):
        if unigrams[lemma][0] < MIN_FREQ:
            del unigrams[lemma]
    return unigrams


def decompound_file(filename, splitter):
    with open(filename, 'r') as file:
        ctr = 0
        for line in file:
            sys.stderr.write("\r line: " + str(ctr) + " ")
            parts = line.split('\t')
            word = parts[0]
            if word.rstrip():
                sys.stdout.write(word + "\t")
                sys.stdout.write(re.sub(" ", "\t", splitter.main_split(word) + "\n"))
            ctr += 1
    sys.stderr.write("\n")


def decompound_interactive(splitter):
    testwords = ['Herzinf', 'Abreißkalender', 'Abrissarbeit', 'Himmelskörper', 'Himmelskörperkalender',
                 'Tischbein', 'Achsbruch', 'Urinsekten', 'Freitag', 'Deutschland', 'Vereinbart',
                 'Aktionsplan', 'Schweigeminute', 'Donaudampfschifffahrtskapitänsmütze', 'Häuserreihe',
                 'Kirchhof', 'Südwind']
    for w in testwords:
        print(w)
        print(splitter.main_split(w))
        print("---")
    while w != 'exit':
        w = input('Input Compositum: ')
        print(splitter.main_split(w))
        print("---")


def find_possible_splits(splitter):
    w = ""
    while w != 'exit':
        w = input('Input word: ')
        splitter.find_possible_segmentations(w)


def decompound_sentences(splitter, sent):
    retstr = ""
    for w in nltk.word_tokenize(sent):
        sys.stdout.write(splitter.main_split(w))


def decompound_interactive_sentence(splitter):
    test_sent = ["Am Freitag gingen wir bei Südwind zu der Häuserreihe der Konfliktsparteien.",
                 "Die beiden offenen Studien und vier der Einfachblindstudien verwendeten jedoch von Investigatoren durchgeführte Endoskopie als Endpunkt, was möglicherweise den Schutz vor Verzerrungen bietet."]
    for s in test_sent:
        print(s)
        print(splitter.sentence_split(s))
        print("---")

    while s != 'exit':
        s = input('Input Sentence: ')
        print(splitter.sentence_split(s))
        print("---")


t_regex = re.compile(r'(.*)(<t>|<h>)(.*)(</t>|</h>)(.*)')
h_regex = re.compile(r'(.*)<t>(.*)</t>(.*)')
def decompound_entailment_file(xml_file, splitter):
    with open(xml_file, 'r') as file:
        for line in file:
            line = line.rstrip()
            m = t_regex.match(line)
            if m:
                sys.stdout.write(m.group(2))
                for w in nltk.word_tokenize(m.group(3)):
                    sys.stdout.write(" ")
                    sys.stdout.write(splitter.main_split(w).replace("\t", " "))
                sys.stdout.write(" "+m.group(4)+"\n")
            else:
                print(line)

