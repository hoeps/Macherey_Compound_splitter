import regex as re
import sys

PREFIXES = "all|auf|an|ab|zu|weg|aus|dar|durch|ein|fort|her|nach|um|vor"
re_prefix = re.compile(r'(.*)(' + PREFIXES + r')(\|)(.*)')


def merge_prefix(s):
    match = re_prefix.match(s)
    while match:
        s = match.group(1) + match.group(2) + match.group(4)
        match = re_prefix.match(s)
    return s


ctr_non_compounds = 0
ctr_compounds = 0

with open('wmt2007.linguistic_gold_standard.Cap.txt') as file:
    for line in file:
        line = line.rstrip()
        if line:
            parts = line.split("\t")
            if len(parts) == 1:
                #if ctr_non_compounds<=1100:
                if True:
                    ctr_non_compounds += 1
                    print(parts[0] + "\t" + parts[0])
            else:
                ctr_compounds += 1
                compound_parts = merge_prefix(parts[1])
                compound_parts = compound_parts.replace("|", "\t")
                print(parts[0] + "\t" + compound_parts)

sys.stderr.write("non compounds: " + str(ctr_non_compounds)+"\n")
sys.stderr.write("compounds: " + str(ctr_compounds)+"\n")
