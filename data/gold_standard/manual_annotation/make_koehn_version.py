import regex as re
import sys
sys.path.append('/home/rh/Studium/Masterarbeit/implementations/Macherey_Compound_splitter')
from rules import MorphoRule

ALLOWED_FILLERS = set(['s', 'es', 'n', 'en', '-'])

def delete_empty(l):
    ret = []
    for e in l:
        if e != "<empty>":
            ret.append(e)
    return ret


regex_rule = re.compile(r'(.*)/(.*)')
regex_pipe = re.compile(r'(.*)\|(.*)')
with open("my_gold.txt") as file:
    for line in file:
        line = line.rstrip()
        if line:
            result = []
            parts = line.split("\t")
            for i, part in enumerate(parts):
                match_pipe = regex_pipe.match(part)
                if match_pipe:
                    part = match_pipe.group(1)
                    if not regex_rule.match(part) and match_pipe.group(2)[0].swapcase() == part[0]:
                        part += "|"+part[0].swapcase()+part[1:]

                match = regex_rule.match(part)
                if match:
                    if match.group(1) == "" and match.group(2) in ALLOWED_FILLERS:
                        result.append("<empty>")
                    else:
                        r = MorphoRule.from_string(part)
                        if r:
                            result[i - 1] = r.apply(result[i - 1])
                            result.append("<empty>")
                else:
                    result.append(part)
            result = delete_empty(result)
            print("\t".join(result))



