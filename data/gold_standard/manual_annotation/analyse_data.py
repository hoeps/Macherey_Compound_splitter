import sys
from nltk.tokenize import word_tokenize
import regex as re

up_to_line=int(sys.argv[1])
compounds=set()

with open ("compounds", "r") as compoundfile:
    for line in compoundfile:
        if line.lstrip():
            parts=line.split("\t")
            compounds.add(parts[0])


re_num = re.compile(r'(.*)\d(.*)')
re_punc = re.compile(r'(.*)\p{p}(.*)')
def check(w):
    if re_num.match(w): return False
    if re_punc.match(w): return False
    return True


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

compound_ctr = 0.0
non_compound_ctr = 0.0
line_ctr=0
with open("../../himl-test-2015/himl.testing.de.sgm", 'r') as file:
    for line in file:
        if line_ctr<=up_to_line:
            line=cleanhtml(line)
            for word in word_tokenize(line):
                if check(word):
                    if word in compounds:
                        print (word)
                        compound_ctr += 1
                    else:
                        non_compound_ctr += 1
        line_ctr+=1

print("found", compound_ctr, "compounds. ->", compound_ctr/(compound_ctr+non_compound_ctr), "%")

