non_compounds = set()

with open("non_compounds") as file:
    for line in file:
        non_compounds.add(line.rstrip())

for w in non_compounds:
    if w:
        print (w,w,sep="\t")

compounds = set()

with open("compounds") as file:
    for line in file:
        parts = line.split("\t")
        word = parts[0]
        if word not in compounds:
            print (line.rstrip())
            compounds.add(word)