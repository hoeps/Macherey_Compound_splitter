from collections import Counter

op_counts=Counter()
with open("compounds", 'r') as file:
    for line in file:
        parts = line.split("\t")
        for part in parts:
            if "/" in part:
                if "|" in part:
                    ops = part.split("|")
                    for op in ops:
                        op_counts[op]+=1
                else:    
                    op_counts[part]+=1

print(op_counts)