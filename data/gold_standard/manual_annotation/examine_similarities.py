import gensim
import random
import numpy as np

print("Loading word embeddings ...")
embeddings_path = '/home/rh/Studium/Masterarbeit/implementations/Macherey_Compound_splitter/embedding/embeddings_5M'
model = gensim.models.Word2Vec.load(embeddings_path)

gold_file = "my_gold_koehn.txt"
#gold_file = "/home/rh/Studium/Masterarbeit/Data/Cap/cap_processed.txt"

all_words = set()
with open(gold_file, 'r') as file:
    for line in file:
        line = line.lstrip().rstrip()
        parts = line.split("\t")
        if len(parts)>2:
            compound=parts[0]
            for part in parts[1:]:
                all_words.add(part)

sim_to_parts=[]
sim_to_random=[]
sim_start=[]
sim_mid=[]
sim_end=[]

with open("my_gold_koehn.txt", 'r') as file:
    for line in file:
        line = line.lstrip().rstrip()
        parts = line.split("\t")
        if len(parts)>2:
            compound=parts[0]
            i = 0
            for part in parts[1:]:
                if compound in model.wv.vocab and part in model.wv.vocab:
                    sim = model.similarity(compound, part)
                    rand_word = random.sample(all_words, 1)[0]
                    while rand_word == part or (rand_word not in model.wv.vocab):
                        rand_word = random.sample(all_words, 1)[0]

                    print("sim to part:", compound, part, sim, sep=" ")
                    sim_to_parts.append(sim)
                    if i == 0:
                        sim_start.append(sim)
                    elif i == len(parts[1:])-1:
                        sim_end.append(sim)
                    else:
                        sim_mid.append(sim)
                    i += 1
            if compound in model.wv.vocab:
                sim_random = model.similarity(compound, rand_word)
                sim_to_random.append(sim_random)
                print("sim to random:", compound, rand_word, sim_random, sep=" ")
                print("---------------")


print("Average part similarity:", np.average(sim_to_parts), "(based on", len(sim_to_parts), "samples)", sep=" ")
print("Average random similarity:", np.average(sim_to_random), "(based on", len(sim_to_random), "samples)", sep=" ")
print("Average start similarity:", np.average(sim_start), "(based on", len(sim_start), "samples)", sep=" ")
print("Average mid similarity:", np.average(sim_mid), "(based on", len(sim_mid), "samples)", sep=" ")
print("Average end similarity:", np.average(sim_end), "(based on", len(sim_end), "samples)", sep=" ")