import sys
from nltk.tokenize import word_tokenize
from collections import defaultdict
import regex as re

MIN_LENGTH = 3
MINFREQ = 3

if len(sys.argv) != 2:
    print("Usage: python3 extract_unigrams.py path/to/himl_5M.de.tok.true > unigrams_5M.txt")

def load_stopwords():
    stopwords = set()
    with open('stopwords.txt') as file:
        for line in file:
            w = line.rstrip()
            stopwords.add(w)
    return stopwords

re_num = re.compile(r'(.*)\d(.*)')
re_punc = re.compile(r'(.*)\p{p}(.*)')
stopwords = load_stopwords()

def check_de(inputString):
    if re_num.match(inputString): return False
    if re_punc.match(inputString): return False
    if len(inputString) < MIN_LENGTH: return False
    if inputString in stopwords: return False
    if re_multCap.match(inputString): return False
    return True


def print_unigrams(unigrams, minfreq=0):
    unigrams_view = [(v, k) for k, v in unigrams.items()]
    unigrams_view.sort(reverse=True)  # natively sort tuples by first element
    for v, k in unigrams_view:
        print(k,v, sep="\t")
        if int(v)<minfreq:
            return


def get_unigrams(filename):
    unigrams = defaultdict(int)
    with open(filename) as file:
        for line in file:
            words=line.split(" ")
            for w in words:
                w=w.rstrip()
                #w = w.lower()
                if check_de(w.lower()):
                    unigrams[w] += 1
    return unigrams


def truecased_unigrams(unigrams):
    truecased_unigrams = {}
    for w in unigrams.keys():
        if len(w)>0:
            if w.lower() in truecased_unigrams:
                if unigrams[w]>truecased_unigrams[w.lower()][0]:
                    truecased_unigrams[w.lower()] = [unigrams[w], w]
            else:
                truecased_unigrams[w.lower()] = [unigrams[w], w]
    return truecased_unigrams


def print_truecased_unigrams(unigrams, minfreq=1):
    unigrams_view = [(v, k, truecased) for k, [v, truecased] in unigrams.items()]
    unigrams_view.sort(reverse=True)  # natively sort tuples by first element
    for v, k, truecased in unigrams_view:
        #if k=='himmel':
         #   print ("himmel")
        if int(v)<minfreq:
            return
        print(k,v,truecased, sep="\t")

re_multCap = re.compile(r'[A-Z].*[A-Z]')
def final_filtering(truecased_unigrams):
    filtered_unigrams = {}
    for word, [count, truecased] in truecased_unigrams.items():
        keep = True
        if re_multCap.match(truecased): keep=False
        if keep: filtered_unigrams[word]=[count, truecased]
    return filtered_unigrams

unigrams = get_unigrams(sys.argv[1])
truecased_unigrams = truecased_unigrams(unigrams)
filtered_unigrams = final_filtering(truecased_unigrams)
print_truecased_unigrams(filtered_unigrams, minfreq=MINFREQ)

