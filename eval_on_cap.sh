#!/usr/bin/env bash

CAPCOUNTS="data/unigrams/europarl_unigrams.txt"
MEDICALCOUNTS="data/unigrams/unigrams_himl_medical.txt"

CAPEVAL="data/gold_standard/gold_cap.txt"
MEDICALEVAL="data/gold_standard/gold_medical.txt"

EMBED_EUROPARL="--embed_file=embedding/embeddings_europarl"
EMBED_MEDICAL="--embed_file=embedding/embeddings_medical"

rm -f eval/macherey_output_cap.txt

python3 main.py --split_mode=lemmata $EMBED_MEDICAL --rules=langer --counts=$CAPCOUNTS --on_file=$CAPEVAL > eval/macherey_output_cap.txt
python3 eval/eval.py eval/macherey_output_cap.txt --gold_standard=$CAPEVAL



